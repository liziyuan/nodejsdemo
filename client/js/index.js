/**
 * Created by ziyuan_li on 2016/4/18.
 */

$(function(){
    function resolve(data){
        var $body = $('.content');
        var cItem = '<tr><td>[ID]</td><td>[NAME]</td></tr>';
        var content = '';
        for(var i= 0,max = data.length;i<max;i++){
            var item = data[i];
            var dom = cItem.replace('[ID]',(item.id).toString()).replace('[NAME]',item.name);
            content+=dom;
        }

        $body.append($(content));
    };
    $('#button').on('click',function(event){
        event.preventDefault();
        $.ajax({
            url: 'http://localhost:8808/user/list',
            type: 'post',
            data:$('form').serialize(),
            dataType: 'json',
            success:function(data){
                console.log(data);
                resolve(data);
            },
            error:function(error){
                alert('error');
            }
        });
    });
});

