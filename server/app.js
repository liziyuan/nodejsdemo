/**
 * Created by ziyuan_li on 2016/4/18.
 */

var fs = require('fs');
var express = require('express');
var path = require('path');
//var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var user = require('./routers/user');
var app = express();

app.set('views',path.join(__dirname,'views'));
app.set('view engine','html');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.all('*',function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With , yourHeaderFeild');
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');

    if (req.method == 'OPTIONS') {
        res.send(200); /��options������ٷ���/
    }
    else {
        next();
    }
});
app.get('/index',function(request,response){
    console.log(request.body);
    var userList = [
        {id:1,name:'zhangsan'},
        {id:2,name:'lisi'}
    ];
    response.json(userList);
});
app.use('/user',user);
app.listen('8808');